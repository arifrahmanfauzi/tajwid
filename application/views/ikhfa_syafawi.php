<div class="ikhfasyafawi">
  <p><strong>Ikhfa' Syafawi</strong></p>
  <p><span style="font-weight: 400;">Apabila mim mati (مْ) bertemu dengan ba (ب)</span></p>
  <p><span style="font-weight: 400;">maka cara membacanya harus dibunyikan samar-samar di bibir dan didengungkan dengan panjang 2 harakat.</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Ikhfa' Syafawi : </p>
  <p><img src = 'assets/foto/tajwid mim/ikhfa syafawi/ikfaSyafawi1.png'></p>
  <p><img src = 'assets/foto/tajwid mim/ikhfa syafawi/ikfaSyafawi2.png'></span></p>
  </div>