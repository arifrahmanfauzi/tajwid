<div class="idgham_mimmi">
  <p><strong>Idgham Mimmi</strong></p>
  <p><span style="font-weight: 400;">Apabila mim mati (مْ) bertemu dengan mim (مْ), maka cara membacanya adalah seperti menyuarakan mim rangkap atau ditasyidkan dan wajib dibaca dengung.</span></p>
  <p><span style="font-weight: 400;">Idgham mimi disebut juga idgham mislain atau mutamasilain.</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Idgham Mimmi :</span></p>
  <p><img src = 'assets/foto/tajwid mim/idhgam_mimi/mimi1.png'></p>
  <p><img src = 'assets/foto/tajwid mim/idhgam_mimi/mimi2.png'></p>
  </div>