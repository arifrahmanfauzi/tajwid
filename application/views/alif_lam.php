<div class="alif_lam">
  <p><strong>Alif Lam</strong></p>
  <p><span style="font-weight: 400;">Dalam ilmu tajwid dikenal hukum bacaan alif lam ( ال ).</span>
  <p><span style="font-weight: 400;">Hukum bacaan alim lam  ( ال) menyatakan bahwa apabila huruf alim lam ( ال ) bertemu dengan huruf-huruf hijaiyah, maka cara membaca huruf alif lam ( ال ) tersebut terbagi atas dua macam, yaitu alif lam ( ال ) syamsiyah dan alif lam ( ال ) qamariyah</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Pada Hukum Alif Lam terdapat 2(dua hukum) yaitu :</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Al-Syamsiyah</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Al-Qamariyah</span></p>
  <p></p>
  <p></p>
  <p><strong>Al Syamsiyah</strong></p>
  <p><span style="font-weight: 400;">“Al” Syamsiyah adalah “Al” atau alif lam mati yang bertemu dengan salah satu huruf syamsiyah dan dibacanya lebur/idghom (bunyi “al’ tidak dibaca).</span>
  <p><span style="font-weight: 400;">Ciri-ciri hukum bacaan “Al” Syamsiyah: . Dibacanya dileburkan/idghom, Ada tanda tasydid/syiddah diatas huruf yang terletak setelah alif lam mati => الـــّ</span></p>
  <p></p>
  <p></p>
  <p><strong>Al Qamariyah</strong></p>
  <p><span style="font-weight: 400;">“Al” Qamariyah adalah “Al” atau alif lam mati yang bertemu dengan salah satu huruf qamariyah dan dibacanya jelas/izhar.</span>
  <p><span style="font-weight: 400;">Huruf-huruf tersebut adalah :  ا ب ج ح خ ع غ ف ق ك م و ه ي</span></p>
  <p><span style="font-weight: 400;">Ciri-ciri hukum bacaan “Al” Qamariyah : Dibacanya jelas/izhar, Ada tanda sukun ( ْ ) di atas huruf alif lam mati => الْ</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Alif Lam : </p>
  <p><img src = 'assets/foto/lam/alif lam qamariyah/qamariyah1.png'></p>
  <p><img src = 'assets/foto/lam/alif lam syamsiyah/syamsiyah1.png'></span></p>
 </div>