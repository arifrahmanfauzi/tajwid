<div class="content">
  <p><strong>Ikhfa'</strong></p>
  <p><span style="font-weight: 400;">Ikhfa artinya adalah menyamarkan / tidak jelas </span></p>
  <p><span style="font-weight: 400;">Apabila bertemu dengan nun mati / tanwin bertemu dengan salah satu huruf ikhfa yang 15 (ت ث ج د ذ س ش ص ض ط ظ ف ق ك ), maka dibacanya samar-samar, antara jelas dan tidak (antara izhar dan idgham) dengan mendengung.</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Ikhfa' :<img src = 'assets/foto/nun/ikhfa/ikhfa1.png'><img src = 'assets/foto/nun/ikhfa/ikhfa2.png'></span></p>
  <p></p>
</div>
