<div class="mad">
  <p><strong>What is Maad ??</strong></p>
  <p><span style="font-weight: 400;">Maad adalah ...... </span></p>
  <p><span style="font-weight: 400;">Memanjangkan suara suatu bacaan</span></p>
  <p> <span style="font-weight: 400;">Sedangkan Maad Sendiri Dibagi menjadi beberapa Hukum Maad yang lain</span></p>
  <p>&nbsp;</p>
</div>

<div class="Mad Thobii">
  <p><strong>1. Mad Thobi'i</strong></p>
  <p><span>Mad Thobi'i terjadi apabila :</p>
  <p>- Huruf berbaris Fathah bertemu dengan alif</p>
  <p>- Huruf berbaris kasroh bertemu dengan ya mati</p>
  <p>- Huruf berbaris dhommad bertemu dengan wawu mati</span></p>
  <p><strong> Panjangnya adalah 1 alif atau 2 harokat</strong></p>

  <p>Contoh : <img src = 'assets/foto/mad/mad thobi.png'></p>

  

</div>

<div class="Mad Wajib muttashil">
  <p><strong>2. Mad Wajib muttashil</strong></p>
  <p><span>Yaitu setiap mad thobi’i bertemu dengan hamzah dalam satu kata.</p>
  <p>Panjangnya adalah 5 harokat atau 2,5 alif. (harokat = ketukan/panjang setiap suara)</span></p>

  <p> Contoh : <img src =  'assets/foto/mad/mad wajib mutashil.png'></p>

  

</div>

<div class ="Mad Jaiz munfashil">
  <p><strong>3. Mad Jaiz Munfashil</strong></p>
  <p><span>Yaitu setiap mad thobi’i bertemu dengan hamzah dalam kata yang berbeda.</p>
  <p>Panjangnya adalah 2, 4, atau 6 harokat (1, 2, atau 3 alif).<span></p>

  <p> Contoh : <img src = 'assets/foto/mad/mad jaiz munfashil.png'></p>

 

</div>

<div class ="Mad Aridh Lisukuun">
  <p><strong>4. Mad Aridh Lisukuun</strong></p>
  <p><span>Yaitu setiap mad thobi’i bertemu dengan huruf hidup dalam satu kalimat dan dibaca waqof (berhenti).</p>
  <p>Panjangnya adalah 2, 4, atau 6 harokat (1, 2, atau 3 alif).  Apabila tidak dibaca waqof, maka hukumnya kembali seperti mad thobi’i.</span></p>

  <p> Contoh : <img src = 'assets/foto/mad/mad aridh lissukun.png'></p>

 

</div>

<div class ="Mad Badal">
  <p><strong>5. Mad Badal</strong></p>
  <p><span>Yaitu mad pengganti huruf hamzah di awal kata. Lambang mad madal ini biasanya berupa tanda baris atau kasroh tegak.</p>
  <p>Panjangnya adalah 2 harokat (1 alif)</span></p>

  <p> Contoh : <img src = 'assets/foto/mad/mad badal.png'></p>

  

</div>

<div class ="Mad iwad">
<p><strong>6. Mad iwad</strong></p>
<p><span>Yaitu mad yang terjai apabila pada akhir kalimat terdapat huruf yang berbaris fathatain dan dibaca waqof.</p>
<p>Panjangnya 2 harokat (1 alif).</span></p>

<p> Contoh : <img src = 'assets/foto/mad/mad iwad.png'></p>



</div>

<div class ="Mad Lazim mutsaqqol kalimi">
<p><strong>7. Mad Lazim Mutsaqqol Kalimi</strong></p>
<p><span>Yaitu bila mad thobi’i bertemu dengan huruf yang bertasydid.</p>
<p>Panjangnya adalah 6 harokat (3 alif).</span></p>

<p> Contoh : <img src = 'assets/foto/mad/mad lazim musaqol kalimi.png'></p>



</div>

<div class ="Mad lazim mukhoffaf kalimi">
<p><strong>8. Mad Lazim Mukhoffaf Kalimi</strong></p>
<p><span>Yaitu bila mad thobi’i bertemu dengan huruf sukun atau mati.</p>
<p>Panjangnya adalah 6 harokat (3 alif).</span></p>

<p> Contoh : <img src = 'assets/foto/mad/mad lazim mukhoffaf kalimi.png'></p>


</div>

<div class ="Mad Lazim mukhoffaf harfi">
<p><strong>9. Mad Lazim Mukhoffaf Harfi</strong></p>
<p><span>Mad ini juga terjadi hanya pada awal surat dalam al-qur’an. Huruf mad ini ada lima dan Panjangnya adalah 2 harokat.</p>

<p> Yaitu : <img src = 'assets/foto/mad/mad lazim mukhoffaf harqi.png'></p>
<p> Contoh : <img src = 'assets/foto/mad/mad lazim mukhofaf harqi.png'></p>



</div>

<div class ="Mad Layyin">
<p><strong>10. Mad Layyin</strong></p>
<p><span>Mad ini terjadi bila :</p>
<p>huruf berbaris fathah bertemu wawu mati atau ya mati, kemudian terdapat huruf lain yg juga mempunyai baris. Mad ini terjadi di akhir kalimat kalimat yang dibaca waqof (berhenti).</p>
<p>Panjang mad ini adalah 2 – 6 harokat ( 1 – 3 alif).</span></p>

<p> Contoh : <img src = 'assets/foto/mad/mad layyin.png'></p>



</div>

<div class ="Mad Shillah">
<p><strong>11. Mad Shillah</strong></p>
<p>Mad ini terjadi pada huruh “ha” di akhir kata yang merupakan dhomir muzdakkar mufrod lilghoib (kata ganti orang ke-3 laki-laki).</p>
<p>Syarat yang harus ada dalam mad ini adalah bahwa huruf sebelum dan sesudah “ha” dhomir harus berbaris hidup dan bukan mati/sukun.</p>
<p> Mad Shillah terbagi menjadi 2, Yaitu : </p>
<p><strong> A. Mad Shillah Qashiroh </strong></p>
<p><span>Terjadi bila setelah “ha” dhomir terdapat huruf selain hamzah. Dan biasanya mad ini dilambangkan dengan baris fathah tegak, kasroh tegak, atau dhommah terbalik pada huruf “ha” dhomir.</p>
<p>Panjangnya adalah 2 harokat (1 alif).</span></p>

<p> Contoh : <img src = 'assets/foto/mad/mad shilah qosiroh.png'></p>
<p><strong> B. Mad Shillah Thowillah </strong></p>
<p><span> Terjadi bila setelah “ha” dhomir terdapat huruf hamzah.</p>
<p>Panjangnya adalah 2-5 harokat (1 – 2,5  alif).</span></p>
<p> Contoh : <img src = 'assets/foto/mad/mad Shilah towilah.png'></p>


<div class ="Mad Farqu">
<p><strong>12. Mad Farqu</strong></p>
<p><span>Terjadi bila mad badal bertemu dengan huruf yang bertasydid dan untuk membedakan antara kalimat istifham (pertanyaan) dengan sebuutan/berita.</p>
<p>Panjangnya 6 harokat.</span></p>

<p> Contoh : <img src = 'assets/foto/mad/mad farqu.png'></p>
</div>

<div class ="Mad Tamkin">
<p><strong>13. Mad Tamkin</strong></p>
<p><span>Terjadi bila 2 buah huruf ya bertemu dalam satu kalimat, di mana ya pertama berbaris kasroh dan bertasydid dan ya kedua berbaris sukun/mati.</p>
<p>Panjangnya 2 – 6 harokat (1 – 3 alif).</span></p>

<p>Contoh : <img src = 'assets/foto/mad/mad tamkin.png'></p>



</div>

