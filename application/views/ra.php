<div class= "ra">
	<p><strong>Hukum Membaca Ra</strong></p>
	<p><span style="font-weight: 400;">hukum bacaan Ra terbagi menjadi tiga,yaitu:</span></p><br>

	<p><strong>A. Ra Dibaca Tafkhim </strong></p>
	<p><span style="font-weight: 400;">menurut bahasa adalah tebal , sedangkan menurut istilah Tafkhim adalah menebalkan huruf tertentu dengan cara mengucapkan huruf tertentu dengan cara mengucapkan huruf di bibir (mulut) dengan menjorokkan ke depan (bahasa Jawa mecucu).Cara membacanya yaitu dengan bibir sedikit kemuka atau monyong.</span></p><br>

	<p><strong>Ra tafkhim (tebal) apabila keadaannya sbb:</strong></p>
	<p><span style="font-weight: 400;">1. Ra berharkat fathah 
		2. Ra berharkat dhummah 
		3. Ra diwakafkan sebelumnya huruf yang berharkat fathah atau Dhummah 
		4. Ra sukun sebelumnya huruf yang berbaris fathah atau dhummah
		5. Ra sukun karena wakaf sebelumnya terdapat alif atau wau yang mati 
		6. Bila ra terletak sesudah Hamzah Washal
	</span></p><br>

	<p><strong>B. Ra Dibaca Tarqiq</strong></p>
	<p><span style="font-weight: 400;">Menurut bahasa  adalah tipis sedangkan menurut istilah Tarqiq adalah membunyikan huruf-huruf tertentu dengan suara atau bacaan tipis. dan Cara membacanya yaitu dengan menarik bibir sedikit mundur sehingga agak meringis.</span></p><br>

	<p><strong>Ra tarqiq (tipis) apabila keadaannya sebagai berikut:</strong></p>
	<p><span style="font-weight: 400;">Ra dibaca Tarqiq bila:
		1.Ra berharkat kasrah   
		2. Ra sukun sebelumnya huruf berharkat kasrah dan sesudahnya bukanlah huruf Ist’la’
		3. Ra sukun sebelumnya huruf yan berharkat kasrah dan sesudahnya huruf Ist’la’ dalam kata yang terpisah.  
		4. Ra sukun karena wakaf, sebelumnya huruf berharkat kasrah atau ya sukun. 
		
		5. Ra sukun karena wakaf sebelumnya bukan huruf huruf Isti’la’dan sebelumnya didahului oleh huruf yang berbaris kasrah. 

		Catatan:huruf Isti’lak ialah melafalkan huruf dengan mengangkat pangkal lidah kelangit-langit yang mengakibatkan hurfnya besar ق ص ض ظ ط غ خ
	</span></p><br>

	<p><strong>C. Ra Boleh Dibaca Tafkhim Atau Tarqiq</strong></p>
	<p><span style="font-weight: 400;">Ra dibaca tarkik dan tafkhim bila:
		1. Ra sukun sebelumnya berharkat kasrah dan sesudahnya huruf Isti’la’ berharkat kasrah atau Kasratain. 
		2. Ra sukun karena wakaf, sebelumnya huruf Isti’la’ yang berbaris mati, yang diawali dengan huruf yang berharkat kasrah.
	</span></p><br>
	<p></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Ra' :</p>
  <p><img src = 'assets/foto/ra/ratafkim/tafkhim1.png'></p></p>
  <p><img src = 'assets/foto/ra/ratarqiq/tarqiq1.png'>
</div>