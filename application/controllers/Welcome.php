<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function idzhar(){
		$this->load->view('idzhar');
	}
	public function ikhfa(){
		$this->load->view('ikhfa');
	}
	public function idgham(){
		$this->load->view('idgham');
	}
	public function advanced(){
		$this->load->view('advanced');
	}
	public function isymam(){
		$this-load-view('isymam');
	}
	public function iqlab(){
		$this->load->view('iqlab');
	}

	public function ikhfa_syafawi(){
		$this->load->view('ikhfa_syafawi');
	}
	public function idgham_mimmi(){
		$this->load->view('idgham_mimmi');
	}
	public function idzhar_syafawi(){
		$this->load->view('idzhar_syafawi');
	}
	public function qalqalah(){
		$this->load->view('qalqalah');
	}
	public function ra(){
		$this->load->view('ra');
	}
	public function mad(){
		$this->load->view('mad');
	}
	public function alif_lam(){
		$this->load->view('alif_lam');
	}
	public function article(){
		$this->load->view('article');
	}
}



